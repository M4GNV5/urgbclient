# URGB Client
A C# Libary and Example Application for the Famous and widely used Server Libary NeoPixel https://github.com/hopfenspace/esp32-neopixel-server, in Combination with the best of all RGB Controller Boards, the URGB LED Controller.

## Architecture
The Client can control multiple URGB Controller Boards, each at each URGB Controller Board multiple LED Strips can be attached.
```
Client -> List<Server> -> List<Strips> -> List<LED>
```
## Communication
UDP is the only Network Protocol used by the URGB Controller. Therefore when e.g. some commands not executed as expected, try the command again and check the Wifi Connectivity of the URGB Controller.

## Getting Started

Create a Server and Connect with local Wifi:
```
var server = Client.Instance.AddServer("NAME","SERVERIP",1337);
server.SetWifi("SSID","Password");
server.SetWifiMode(WifiMode.STA);
server.Reboot();
```
Get Server by Name:
```
Server server = Client.Instance.Get("NAME");
List<Server> servers = Client.Instance.GetAll();
```

Create a LED Strip:
```
// Name can be anything. second Parameter is the Strip Length
var stripe = server.CreateStrip("NAME",200);

```
Get Stripe by Name:
```
Stripe stripe = server.Get("NAME");
List<Stripe> stripes = server.GetAll();
```


Execute some basic Commands:
```
// Sets LED with Idx 10 to red
stripe.SetLed(10,Color.Red);
await Task.Delay(1000);
// Complete Stripe to Blue
stripe.SetStripe(Color.Blue);
await Task.Delay(1000);
// Sets the first 10 LED to Green
stripe.SetLeds(0,10,Color.Green);
```




