using System;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Communication
{
    public class Communicator
    {
        public string Host { get; private set; }
        private readonly UdpClient _client;
        private byte[] sendBuffer = new byte[1024];
        private int bufferLength = 0;

        public Communicator(string host)
        {
            Host = host;
            _client = new UdpClient(1337);
            _client.Connect(Host,1337);
        }

        public async Task Flush()
        {
            await SendData(new byte[]{0}, true);
        }

        public async Task SetPixel(short ledIdx, Color color)
        {
            var command = new byte[6];
            var idx = BitConverter.GetBytes(ledIdx);
            command[0] = 0x01;
            command[1] = idx[0];
            command[2] = idx[1];
            command[3] = color.R;
            command[4] = color.G;
            command[5] = color.B;
            await SendData(command);
        }

        public async Task FillPixel(short offset, short length, Color color)
        {
            var command = new byte[8];
            var idx = BitConverter.GetBytes(offset);
            var len = BitConverter.GetBytes(length);
            command[0] = 0x02;
            command[1] = idx[0];
            command[2] = idx[1];
            command[3] = len[0];
            command[4] = len[1];
            command[5] = color.R;
            command[6] = color.G;
            command[7] = color.B;
            await SendData(command);
        }

        public void SetLedCount(short count)
        {
            var command = new byte[3];
            var cnt = BitConverter.GetBytes(count);
            command[0] = 0x10;
            command[1] = cnt[0];
            command[2] = cnt[1];
        }


        public async Task SetWifiMode(bool ap)
        {
            var command = new byte[2];
            command[0] = 0x20;
            command[1] = (byte) (ap ? 0 : 1);
            await SendData(command);
        }

        public async Task SendWifiSsid(string ssid)
        {
            var command = new byte[2+ssid.Length];
            command[0] = 0x21;
            command[1] = BitConverter.GetBytes(ssid.Length).Last();
            var ssidBytes = Encoding.ASCII.GetBytes(ssid);
            for (var i = 0; i < ssidBytes.Length; i++)
            {
                command[i + 2] = ssidBytes[i];
            }
            await SendData(command);
        }
        
        public async Task SendWifiPassword(string ssid)
        {
            var command = new byte[2+ssid.Length];
            command[0] = 0x22;
            command[1] = BitConverter.GetBytes(ssid.Length).Last();
            var ssidBytes = Encoding.ASCII.GetBytes(ssid);
            for (var i = 0; i < ssidBytes.Length; i++)
            {
                command[i + 2] = ssidBytes[i];
            }
            await SendData(command);
        }

        public async void SendReboot()
        {
            await SendData(new byte[]{0xff}, true);
        }
        
        private async Task<int> SendData(byte[] data, bool flushBuffer = false)
        {
            bool fitsInBuffer = bufferLength + data.Length < sendBuffer.Length;
            if(fitsInBuffer)
            {
                Buffer.BlockCopy(data, 0, sendBuffer, bufferLength, data.Length);
                bufferLength += data.Length;
            }

            if(flushBuffer)
            {
                int ret = await _client.SendAsync(data, bufferLength);
                bufferLength = 0;
                return ret;
            }
            else if(!fitsInBuffer)
            {
                int ret = await _client.SendAsync(data, bufferLength);
                Buffer.BlockCopy(data, 0, sendBuffer, 0, data.Length);
                bufferLength = data.Length;
                return ret;
            }
            else
            {
                return 0;
            }
        }
    }
}